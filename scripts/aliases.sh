# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
    eval "`dircolors -b`"
    alias ls='ls --color=auto'

    alias grep='grep --color=auto'
    alias grepc='grep --color=always'
fi

alias lsl="ls -l"

alias diff="diff -u"

alias svn_st_all_repos="find . -type d -exec test -d {}/.svn \; -print -execdir svn st -q {} \; -prune"
alias hg_st_all_repos="find . -type d -exec test -d {}/.hg \; -print -execdir hg st -q {} \; -prune"

alias dice='python -c "import random; print random.randrange(1,6)"'

alias kill_jobs='kill $(jobs -p)'
