export PYTHONSTARTUP=~/.pythonrc

# cause ack-grep to look for a .ackrc file in the current directory
# e.g. ~/dev/django_projects/gps2/.ackrc contains --ignore-dir=data
export ACKRC=./.ackrc

# set variable identifying the chroot you work in (used in the prompt below)
if [ -z "$debian_chroot" ] && [ -r /etc/debian_chroot ]; then
    debian_chroot=$(cat /etc/debian_chroot)
fi

PS1='${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u@\h [last: ${timer_show}s] \[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\$ '

export WORKON_HOME=$HOME/.virtualenvs
source /usr/local/bin/virtualenvwrapper.sh

export GOPATH=$HOME/dev/go
export PATH=$PATH:$HOME/bin

export EDITOR=/usr/bin/vim

# Hook for desk activation
[ ! -z "$DESK_ENV" ] && source "$DESK_ENV"
export DESK_DESKS_DIR="$HOME/dotfiles/scripts/desks/"
