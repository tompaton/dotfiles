# ~/.bash_aliases

# graphical ls of subdirectory tree
alias gls="ls -R | grep ':$' | sed -e 's/:$//' -e 's/[^-][^\/]*\//–/g' -e 's/^/ /' -e 's/-/|/'"

# apache shortcuts
alias wwwload='sudo service apache2 reload'
alias wwwreset='sudo service apache2 restart'
alias wwwedit='sudo -e /etc/apache2/httpd.conf'

alias ack=ack-grep

#alias hglog_sincedeployed='hg log | awk "{print;} /Deployed-/ {exit;}" | grep -v "^?\s+user:" | grep -v "^?\s+branch:" | grep -v "^?\s+changeset:" | grep -v "^?\s+tag:"'
#alias hglog_sincedeployed='hg glog | awk "{print;} /Deployed-/ {exit;}" | grep -v "^[@|o ]\+\(user\|branch\|tag\|changeset\):"'
#alias hglog_sincedeployed='hg glog | awk "{print;} /Deployed-/ {exit;}"'
#alias hglog_sincedeployed='hg log -r `hg log | grep "\[Deployed-" -m 1 | cut -d[ -f1`:'
alias hglog_sincedeployed='hg log | sed "/Added tag Deployed/d; /Deployed/q; /^  /!d;" | tac'
