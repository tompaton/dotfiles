# ~/.bash_functions

# find all mercurial repositories with uncommitted changes
function hg_all_dirty ()
{
    find /home/tom/ -type d -name ".hg" -execdir hg prompt "{status}" \; -print -prune 2> /dev/null | grep '^!'
}
