# cause ack-grep to look for a .ackrc file in the current directory
# e.g. ~/dev/django_projects/gps2/.ackrc contains --ignore-dir=data
export ACKRC=./.ackrc

export WORKON_HOME=$HOME/.virtualenvs
source /usr/local/bin/virtualenvwrapper.sh

export GOPATH=$HOME/dev/go
export PATH=$PATH:$HOME/bin

export EDITOR=/usr/bin/vim

# Hook for desk activation
[ ! -z "$DESK_ENV" ] && source "$DESK_ENV"
export DESK_DESKS_DIR="$HOME/dotfiles/scripts/desks/"
