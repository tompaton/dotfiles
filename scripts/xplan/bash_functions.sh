color_word ()
{
    word=$1;
    shift;
    sed "s/\($word\)/$(color $@)\1$(color off)/;"
}

# get svn diff and compare added functions to removed functions
svn_diff_defs ()
{
    /bin/env python `cygpath -w ~/svn_diff_defs.py` $@
}

git_diff_defs ()
{
    /bin/env python `cygpath -w ~/dotfiles/scripts/svn_diff_defs.py` __GIT__ $@
}

# list all template $vars in an .odt file
odt_vars ()
{
  # don't use tidy as it discards some variables in the reformatting/validation
  unzip -p $1 | ack -o "([$]\w+)|([%]\w+[%])" | sort | uniq | ack -v "[$](Build|Win32|Windows_x86|\w)\b"
}

odt_to_xml () 
{
    unzip -p $1 | tidy -q -xml -raw -f /dev/nul
}

alias ackodt=/home/tpaton/ackodt.sh

# run svn update on all modified files that have changed on the server
# ignore anything in a changelist
svn_update_modified ()
{
    for upd in `svn status -u | awk '(/^--- Changelist/) { exit; } ($1=="M" && $2=="*") {print $4;}'` ; do 
        svn update "$upd"
    done
}

