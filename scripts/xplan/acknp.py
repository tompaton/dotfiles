#!/bin/env python
"""Wrap a call to ack so output goes to ~/ack.txt and is launched into np++"""

import sys, os

np = "c:/Program Files (x86)/Notepad++/notepad++.exe"
out = "c:/cygwin/home/tpaton/ack.txt"
args = ' '.join('"%s"' % a for a in sys.argv[1:])

open(out, "w").write("acknp: %s\nack %s\n\n" % (os.getcwd(), args))

os.system("ack %s >> %s" % (args, out))

os.system('"%s" %s' % (np, out))
