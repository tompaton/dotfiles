# custom prompt
pwd_branch ()
{
    # color branch if found in prompt
    #pwd | color_word '2.[0-9]\+.999' bold yellow
    #pwd | sed "s/\(2.[0-9]\+.999\)/$(color bold yellow)\1$(color off)/;"
    pwd | sed 's/\/home\/tpaton\//~\//; s/\([0-9]\+.[0-9]\+.[0-9]\+\)/\x1b\[1;33m\1\x1b\[0m/;'
}
svn_branch_info ()
{
    svn info 2> /dev/null | sed -n 's,^.*/svn/\(branches/\)\?\(.*\)/xplan, <\2>,p'
}
git_branch_info ()
{
    git branch 2> /dev/null | sed -n '/^\*/s/^\* \(.*\)/ [\1]/p'
}
PS1="\[\e]0;\w\a\]\n\[\e[32m\]\u@\h \[\e[33m\]\$(pwd_branch)\$(svn_branch_info)\$(git_branch_info)\n\[\e[36m\]\t [last: \${timer_show}s]\[\e[0m\] $ "

# branch directory aliases
alias trunk="cd ~/xplanbase/version/99.99.999"
alias cd225="cd ~/xplanbase/version/2.25.999"
alias cd224="cd ~/xplanbase/version/2.24.999"
alias cd223="cd ~/xplanbase/version/2.23.999"
alias cd222="cd ~/xplanbase/version/2.22.999"
alias cd221="cd ~/xplanbase/version/2.21.999"
alias cd220="cd ~/xplanbase/version/2.20.999"
alias cd219="cd ~/xplanbase/version/2.19.999"
alias cd218="cd ~/xplanbase/version/2.18.999"
alias cd217="cd ~/xplanbase/version/2.17.999"

# execute the given command in each branch's source folder
foreach_branch ()
{
    for branch in 99.99.999 2.21.999 2.22.999 2.23.999 2.24.999 2.25.999 ; do
        cd /home/tpaton/xplanbase/version/$branch
        echo "-- $(pwd) ----------------------------------------------------------------------------"
        $@
        echo
    done
    trunk
}

get_branch ()
{
    # TODO: if in git repo, use current branch to find site name

    local branch="$1"
    if [[ ${branch:0:1} == "-" ]]; then
        branch="" # squelch options that may have got through
    fi
    if [[ -z "$branch" ]]; then
        # pick site name up from pwd (directory name will match version in status)
        branch=`sitectrl status | grep $(basename $(pwd)) | grep -v share_ | grep -v old | grep -v test | cut -d@ -f1`
        branch=`echo $branch` # trim leading spaces
    fi
    if [[ -z "$branch" ]]; then
        branch="trunk"
    fi
    echo $branch
}
