# rr risk service

alias tfs="/cygdrive/c/Program\ Files\ \(x86\)/Microsoft\ Visual\ Studio\ 10.0/Common7/IDE/TF.exe"

meltriskweb_deploy_au ()
{
    local src="/home/tpaton/xplanbase/RiskService/Main/RiskService/AUSNZ/SourceVS2010/IRESS.RiskService/bin/"
    local dst="//MEL-T-RISKWEB1/websites/riskserviceplus/au/bin/"
    
    copy_newer_dlls "$src" "$dst"
    copy_newer_dlls "$src/en-AU/" "$dst/en-AU/"
    
    # trigger reload
    touch "$dst/../Web.config"
}

meltriskweb_deploy_nz ()
{
    local src="/home/tpaton/xplanbase/RiskService/Main/RiskService/AUSNZ/SourceVS2010/IRESS.RiskService/bin/"
    local dst="//MEL-T-RISKWEB1/websites/riskserviceplus/nz/bin/"
    
    copy_newer_dlls "$src" "$dst"
    copy_newer_dlls "$src/en-NZ/" "$dst/en-NZ/"
    
    # trigger reload
    touch "$dst/../Web.config"
}

copy_newer_dlls ()
{
    local src=$1
    local dst=$2
    local startd=$(pwd)
    
    cd "$src"
    for dll in *.dll ; do
        if [ "$dll" -nt "$dst/$dll" ]; then
            if [ -n $3 ]; then
                cd "$dst"
                tfs checkout "$dll"
                cd "$src"
            fi
            cp -v "$dll" "$dst"
        fi
    done
    cd $startd
}

riskservice_release_nz ()
{
    local src="/home/tpaton/xplanbase/RiskService/Main/RiskService/AUSNZ/SourceVS2010/IRESS.RiskService/bin/"
    local dst="/home/tpaton/xplanbase/RiskService/Main/RiskService/AUSNZ/Release/NZ"
    
    copy_newer_dlls "$src" "$dst/bin/" checkout
    copy_newer_dlls "$src/en-NZ/" "$dst/bin/en-NZ/" checkout
    
    cd "$dst"
    rm Web.config
    zip --filesync -r ../NZ_release.zip *
    cd -
}

riskservice_release_au ()
{
    local src="/home/tpaton/xplanbase/RiskService/Main/RiskService/AUSNZ/SourceVS2010/IRESS.RiskService/bin/"
    local dst="/home/tpaton/xplanbase/RiskService/Main/RiskService/AUSNZ/Release/AU"
    
    copy_newer_dlls "$src" "$dst/bin/" checkout
    copy_newer_dlls "$src/en-AU/" "$dst/bin/en-AU/" checkout
    
    cd "$dst"
    rm Web.config
    zip --filesync -r ../AU_release.zip *
    cd -
}

supersolverservice_release_au ()
{
    local src="/home/tpaton/xplanbase/SuperSolverService/SuperSolverService/IRESS.SuperSolverService/bin/"
    local dst="/home/tpaton/xplanbase/SuperSolverService/Release/au"
    
    copy_newer_dlls "$src" "$dst/bin/" checkout
    #copy_newer_dlls "$src/en-AU/" "$dst/bin/en-AU/" checkout
    
    cd "$dst"
    zip --filesync -r ../AU_release.zip *
    cd -
}
