# xplan site manager
alias sitectrl="/home/tpaton/xplanbase/sitemgr/sitectrl"
alias color="/home/tpaton/dotfiles/external/color"
sitectrl_status ()
{
    sitectrl status | color_word RUNNING bold white green | color_word FAILING bold white red | color_word STARTING bold white yellow | color_word UPGRADING bold white yellow
}
alias sitectrl_start="sitectrl start"

running_sites ()
{
    sitectrl status 2> /dev/nul | grep RUNNING | grep -v share_ | cut -d@ -f1
}

sitectrl_kill ()
{
    if [ $# == 0 ]; then
        sites=$(running_sites)
        if [ -n "$sites" ]; then
            sitectrl_kill $sites
        fi
        return $?
    fi

    for site in $@; do
        # stop to ensure a proper cleanup, but don't wait for it to finish as
        # that's a waste of time, so kill after 5 seconds
        (echo "Stopping $site..." &&
             sitectrl stop $site 5) ||
        (echo "Killing $site..." &&
             sitectrl kill $site)
    done
}

# load server.log and stderr.log for the given site
logs ()
{
    branch=$(get_branch $1)
    
    np /home/tpaton/xplanbase/var/$branch/log/stderr.log
    np /home/tpaton/xplanbase/var/$branch/log/server.log
}

log ()
{
    branch=$(get_branch)
    
    np /home/tpaton/xplanbase/var/$branch/log/$1.log
}

last_log_entry ()
{
    branch=$(get_branch $1)
    
    awk '/^\w\w\w [0-9]+ [0-9]+:/ { # reset buffer when we see a timestamp
           n=0
         }
         {
           l[n++]=$0                # add line to buffer
         }
         END {
           for(i=0;i<n;i++) {       # print buffered lines
             print l[i];
           }
         }' /home/tpaton/xplanbase/var/$branch/log/server.log
}

tail_logs ()
{
    branch=$(get_branch $1)
    
    _tail_log /home/tpaton/xplanbase/sitemgr/log/sitemgr.log "[sitemgr]" red
    _tail_log /home/tpaton/xplanbase/var/$branch/log/upgrade.log "  [$branch-upgrade]" yellow
    _tail_log /home/tpaton/xplanbase/var/$branch/log/server.log " [$branch-server]" green
    
    #for l in cxxserver.log external_system_sync.log ips_client_v2.log iress_webservices.log java_startedup.log jserver.log pydeadlock.log security.log shutdown.log startedup.log stderr.log ue2_dual_sync.log; do
    #    _tail_log /home/tpaton/xplanbase/var/$branch/log/$l " [$l]" white
    #done
    
    tail_pids=$(echo $(jobs -p)) # echo to concat pids onto one line
    
    trap "kill $tail_pids" SIGINT EXIT SIGTERM
    wait
    trap - SIGINT EXIT SIGTERM
}
_tail_log ()
{
    prefix="$(color $3)$2$(color off)"
    tail -n 0 -F $1 | sed "s/^/$prefix /" &
}

grep_log ()
{
    log="server"
    if [[ $1 == "--server" ]]; then
        shift
        log="server"
    elif [[ $1 == "--upgrade" ]]; then
        shift
        log="upgrade"
    fi
    
    branch=$(get_branch)
    
    grep "$1" /home/tpaton/xplanbase/var/$branch/log/$log.log
}

# TODO: Get log file from test site
get_test_log ()
{
    server='SP-XPLAN1'
    site='Test_24'
    log='server.log'
    
    cp \\$server.semiprod.iress.com.au\d$\xplanbase\var\$site\log\$log ~/Desktop/$server-$site-$log
    np ~/Desktop/$server-$site-$log
}

wwwreset ()
{
    if [[ $1 == "--url" ]]; then
        shift
        page=$1
        shift
    elif [[ $1 == "--ss" ]]; then
        page="supersolver/scenario_index?clientid=784"
        shift
    elif [[ $1 == "--ic" ]]; then
        #page="engage/investment_choice/scenario_index?client_id=784"
        page="factfind/view/784?role=client&refresh=1"
        shift
    elif [[ $1 == "--sc" ]]; then
        #page="engage/consolidate_super/scenario_index?client_id=784"
        page="factfind/view/784?role=client&refresh=1"
        shift
    elif [[ $1 == "--prem" ]]; then
        page="iqm+/rr/client_details"
        shift
    else
        page="home"
    fi
    
    branch=$(get_branch $1)

    sitectrl_kill $branch

    # truncate server.log
    cp /home/tpaton/xplanbase/var/$branch/log/server.log{,.previous}
    > /home/tpaton/xplanbase/var/$branch/log/server.log

    # tail logs while sitectrl is running
    _tail_log /home/tpaton/xplanbase/sitemgr/log/sitemgr.log "[sitemgr]" red
    _tail_log /home/tpaton/xplanbase/var/$branch/log/upgrade.log "  [$branch-upgrade]" yellow
    _tail_log /home/tpaton/xplanbase/var/$branch/log/server.log " [$branch-server]" green
    
    #for l in scheduler.log cxxserver.log cxxserver_old.log external_system_sync.log ips_client_v2.log iress_webservices.log java_startedup.log jserver.log pydeadlock.log security.log shutdown.log startedup.log stderr.log ue2_dual_sync.log; do
    #    _tail_log /home/tpaton/xplanbase/var/$branch/log/$l " [$l]" white
    #done
    
    #for l in access.log inetd.log server.log shutdown.log startedup.log stderr.log upgrade.log; do
    #    _tail_log /home/tpaton/xplanbase/var/$branch/charts/log/$l " [$l]" white
    #done
    
    tail_pids=$(echo $(jobs -p)) # echo to concat pids onto one line
    
    echo "Starting $branch..."
    sitectrl start $branch &
    
    # kill tails when site starts
    wait ${!}
    kill $tail_pids
    
    sitectrl_status $branch
    
    port=`sitectrl status $branch | grep RUNNING | cut -c 61-64`
    
    if [[ $port ]]; then
        chrome "http://localhost:$port/$branch/$page" &
        disown
    #else
        # show error if we couldn't start the site
        #last_log_entry $branch
    fi
}

export DISABLE_NOTIFIER=true
update_git_and_rebuild ()
{
    if [[ -z $1 ]]; then
        site="--site trunk"
    else
        site="$@"
    fi

    ./xplan stop --all

    echo
    echo "do git bash stuff and hit enter to rebuild"
    echo "(F) to run full make"
    read mode || exit 1

    if [[ "$mode" == "F" ]] || [[ "$mode" == "f" ]]; then
        mode=""
    else
        mode="quick"
    fi

    update_etags &&
        make_xplan $mode &&
        ./xplan run $site
}

make_xplan ()
{
    #if [[ -z "$PASSPHRASE" ]]; then
    #    echo "$(color bold yellow)Stopping - export PASSPHRASE.$(color off)"
    #    return 1
    #fi

    if [[ $1 == "quick" ]]; then
        echo "*** QUICK MAKE ***"
        make -j6 -f GNUmakefile.real idl data src/cxx wsdl
        return $?
    else
        echo "*** FULL MAKE ***"
        make -j6
        return $?
    fi
}

update_branch ()
{
    local branch=$(get_branch $1)
    svn up && update_etags && sitectrl_kill $branch && make -j3 && wwwreset
}

start_sitemgr ()
{
    # fire off sitemanager service too
    explorer ~/xplanbase/sitemgr/aggregator.exe
    explorer ~/xplanbase/sitemgr/sitemgrsrv.exe
}

start ()
{
    start_sitemgr
    trunk && pwd && tsvn && update_branch trunk
}

sql_upgrade_log ()
{
    branch=$(get_branch $1)

    sqlcmd -Q "use xplan$branch; SELECT top 20 * from dbo.upgrade_log $2 order by completed desc"
}

reset_supersolver_zip_import ()
{
    sql_query "select * from config_param where dom='research_supersolver'"
    sql_query "update config_param set data='0' where dom='research_supersolver'"
}

sql_query ()
{
    branch=$(get_branch)
    
    sqlcmd -Q "use xplan$branch; $1"
}

fix_python_egg_permission_error ()
{
    # permissions on pyv8 file cause an issue
    rm -f C:\\Users\\tpaton\\AppData\\Roaming\\Python-Eggs\\pyv8-1.0-py2.7-win32.egg-tmp\\_PyV8.pyd
}

run_tests ()
{
    branch=$(get_branch)
    if [[ $branch == "trunk" ]]; then
        SITENAME=autotest
    else
        SITENAME="${branch/clean/test}"
    fi

    run_tests_ $SITENAME $@
    status=$?

    return $status
}

run_tests_ ()
{
    export XFUDGE_SITENAME="$1"
    shift

    python -m pytest $@
    status=$?
    unset XFUDGE_SITENAME

    return $status
}

log_run_tests ()
{
    run_tests $@ 2>&1 | tee tests.log
    return ${PIPESTATUS[0]}
}

uitests ()
{
    trunk
    cd iress/Test/Xplan/PyXplanTest/1.0/main
    ../../../../../3rdParty/Python/3.4/main/python.exe -i bootstrap.py --test-package tests.supersolver local_config.py
}

autologin_on ()
{
    set_autologin 1 $@ > /dev/nul && echo "Autologin ON ($(autologin_user))"
}
autologin_off ()
{
    set_autologin 0 $@ > /dev/nul && echo "Autologin OFF"
}
autologin_user ()
{
    grep -o "autologin_username\" value=\"\w\+" data/debug.xml | cut -d\" -f 3
}
set_autologin () # [0|1] -u {user}
{
    before=$(grep -o "autologin\" value=\"[01]" data/debug.xml | cut -d\" -f 3)
    user=$(autologin_user)
    
    sed -i "/key=\"autologin\"/ s/value=\".\"/value=\"$1\"/" data/debug.xml
    if [[ $2 == "-u" ]]; then
        sed -i "/key=\"autologin_username\"/ s/value=\"\w\+\"/value=\"$3\"/;" data/debug.xml
    fi
    
    echo "$before -u $user"
}

ws_status ()
{
    # find property definitions
    grep -A 10 "def endpointURIbase\w\+" src/py/xpt/insurance/__init__.py src/py/xpt/supersolver/__init__.py |
    # reformat to remove unneeded paths from grep prefix
    sed 's|src/py/xpt/||; s|/__init__.py[-:]||;' |
    # ignore lines other than def and (uncommented) returns
    grep "^\w\+\s*\(def end\w\+\|return\)" |
    # join lines ending in :
    sed ':a;/:$/{N;s/\n//;ba}' |
    sed 's/def endpointURIbase//; # strip to just locale
         s/(self):\w\+//;
         s/return //;
         s/self.endpointURIbase.\+/DEFAULT/; # label known endpoints
         s/^insurance/insurance  /;' # fix up column alignment
    echo
    
    branch=$(get_branch)
    echo ../../var/$branch/config.d/riskservice.ini
    cat ../../var/$branch/config.d/riskservice.ini
    echo
    echo
    echo ../../var/$branch/config.d/supersolver.ini
    cat ../../var/$branch/config.d/supersolver.ini
    echo
    echo
    
    proxy=$(sql_query "SELECT left(data,50) FROM config_param where dom = 'system' and name = 'http_proxy'" |
            # print only the value line
            sed -n 's/\s//g; 4 p')
    if [ "$proxy" ]; then
        echo "Proxy ==> ON  $proxy"
    else
        echo "Proxy ==> OFF"
    fi
}

# diffstat on all today's changes in trunk
todays_revs ()
{
    local user="$1"
    if [[ -z "$user" ]]; then
        user="tpaton"
    fi
    svn log -l 200 | grep $user | grep `date +%Y-%m-%d` | cut -c 2-7
}

diffstat_revs ()
{
    for r in $@; do
        svn diff -c $r
    done | diffstat
}

xplan_start_server ()
{
    local debug="-N DEBUG"
    if [[ $1 == "--no-debug" ]]; then
        shift
        debug=""
    fi
    local branch=$(get_branch)
    fix_python_egg_permission_error
    python bin/myserver.py -N LOCALE_AU $debug -d local=C:\\xplanbase\\var\\$branch -d var=C:\\xplanbase\\var\\$branch
}

alias etags=/cygdrive/c/emacs-25.3_1-x86_64/bin/etags.exe
update_etags ()
{
    update_etags_js
    update_etags_py
}

update_etags_js ()
{
    _update_etags "data/wwwroot/js" "*.js"
}
update_etags_py ()
{
    _update_etags "src/py/xpt" "*.py"
}

_update_etags ()
{
    cd $1
    find . -name "$2" -print | grep -v "flymake" | grep -v "flycheck" | grep -v "node_modules" | etags - &
    cd -
}

# delete .bak and flymake/flycheck junk files from source
cleanup_src ()
{
    find . -name "*.bak" -print -delete
    find . -name "flycheck-*" -print -delete
    find . -name "*flymake*" -print -delete
}

# watch current branch for changes and reload
reload_when_changed ()
{
    local source=$(cygpath -w -a .)
    local branch=$(get_branch)
    python bin/inotify_reloader.py "$source" --var="c:\\xplanbase\\var\\$branch" $@
}

# manage svn trunk and feature branches

update_feature_branch_from_trunk ()
{
    trunk2
    svn merge https://sydxplansvn.devel.iress.com.au/svn/trunk/xplan && svn commit -m "updated from trunk." && svn up
}

merge_feature_branch_into_trunk ()
{
    trunk
    
    cp data/version data/version.bak
    cp GNUmakefile.preconf GNUmakefile.preconf.bak
    svn revert data/version
    svn revert GNUmakefile.preconf
    
    svn up && svn merge --reintegrate https://sydxplansvn.devel.iress.com.au/svn/branches/supersolver_features/xplan && svn commit -m "Merged supersolver_features into trunk." && svn up
    
    mv data/version.bak data/version
    mv GNUmakefile.preconf.bak GNUmakefile.preconf
}

switch_to_trunk ()
{
    trunk
    sitectrl_kill feature
    sitectrl_kill trunk
    sitectrl upgrade feature 2.88.888
    sitectrl upgrade trunk 2.99.999
    svn switch https://sydxplansvn.devel.iress.com.au/svn/trunk/xplan
}

switch_to_feature ()
{
    trunk
    sitectrl_kill feature
    sitectrl_kill trunk
    sitectrl upgrade feature 2.99.999
    sitectrl upgrade trunk 2.88.888
    svn switch https://sydxplansvn.devel.iress.com.au/svn/branches/supersolver_features/xplan
}

load_hotfix ()
{
    ../bin/compile_hotfix.sh $1 \
        && curl --form process=__all__ \
                --form hotfix=@${1/.py/.pys} \
                --form submit=Upload \
                http://localhost:8080/trunk/hotfix \
        | sed -n '/^\s*<pre>\s*$/,/<\/pre>/p'
}

alias python3=./iress/3rdParty/Python/3.4/main/python.exe
py2to3 ()
{
    python3 ./iress/3rdParty/Python/3.4/main/Tools/Scripts/2to3.py $@
}

py2to3_ss ()
{
    # ignore transformations that are either obsolete or just noise for the purposes of cleaning up code pre python3
    py2to3 -x dict -x callable -x unicode -x basestring $@
}

find_unused_cap_ids ()
{
    diff <(for i in {1..775} ; do echo $i ; done) \
         <(grep -o 'id="[0-9]\+"' data/capabilities.xml | grep -o '[0-9]\+' | sort -g) \
        | grep '^-'
}

wc_ss ()
{
    ( for d in src/py/xpt/supersolver src/py/xpt/resourceful/api/supersolver ; do
          find $d -name "*.py" -exec wc -l {} \;
      done ) | sort -g
}

wc_ss_hist ()
{
    wc_ss | make_hist 100
}

make_hist ()
{
    awk 'BEGIN {FS=" "}
         {x[int($1/BUCKET)]++}
         END {
           for(v in x) {
             bar = sprintf(sprintf("%% %ds", x[v]), "");
             gsub(/ /, "-", bar);
             printf("%4d %4d %s\n", v*BUCKET, x[v], bar);
           }
         }' BUCKET=$1
}

count_ss_imports ()
{
    if [[ -z $1 ]]; then
        # default to filename (minus drive letter)
        field=2
    else
        field=$1
    fi

    sfood-imports src/py/xpt/supersolver/ \
        | awk -v x=$field \
              'BEGIN {FS=":"}
               {
                 if($x ~ /^ \./) {
                   # find full path of relative import
                   parent = " ";
                   l = split($2, y, "\\");
                   for(i=7; i<l; i++) {
                     parent = parent y[i] ".";
                   }
                   print parent substr($x, 3);
                 } else {
                   print $x;
                 }
               }
              ' \
        | sort | uniq -c | sort -g
}

count_ss_imported ()
{
    # import name
    count_ss_imports 4 | grep supersolver
}

ss_imports_hist ()
{
    count_ss_imports | make_hist 1
}

ss_imported_hist ()
{
    count_ss_imported | make_hist 1
}

ss_untested_functions ()
{
    if [[ -z $2 ]]; then
        prefix="test_"
    else
        prefix=$2
    fi
    t="$(dirname $1)/$prefix$(basename $1)"

    for f in $(grep '^def \(\w\+\)(' $1 | sed 's/(/\t/' | awk '{print $2}')
    do
        if [ "" = "$(grep _edit\.$f $t)" ]
        then
            echo TODO $f
        else
            echo TEST $f
        fi
    done
}

gerrit_verify_failed ()
{
    GERRIT="https://sydxplansvn.devel.iress.com.au/gerrit"
    ME="tpaton"
    curl -k "$GERRIT/changes/?q=status:open+owner:$ME&o=LABELS" \
        | python -c "import sys, json
sys.stdin.readline()
for c in json.load(sys.stdin):
    if c['labels'].get('Verified', {}).get('value')==-1:
        print 'verify', c['_number']
"
}
