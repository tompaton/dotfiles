#!/bin/bash
#echo ...gitprepdiff.sh
echo $2
mkdir -p "c:/Users/tpaton/AppData/Local/Temp/GitDiff/Repository/$(dirname $2)"
mkdir -p "c:/Users/tpaton/AppData/Local/Temp/GitDiff/Working/$(dirname $2)"
cp $1 "c:/Users/tpaton/AppData/Local/Temp/GitDiff/Repository/$2"
cp $2 "c:/Users/tpaton/AppData/Local/Temp/GitDiff/Working/$2"
