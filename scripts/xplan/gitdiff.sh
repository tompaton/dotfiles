#!/bin/bash
#echo Running gitdiff.sh...

DIFFTEMP=/cygdrive/c/Users/tpaton/AppData/Local/Temp/GitDiff

echo Deleting and re-creating $DIFFTEMP...
rm -rf $DIFFTEMP
mkdir $DIFFTEMP

#echo Creating $DIFFTEMP/Repository...
mkdir $DIFFTEMP/Repository

#echo Creating $DIFFTEMP/Working...
mkdir $DIFFTEMP/Working

git diff --name-only "$@" | while read filename; do
    git difftool "$@" --no-prompt "$filename" 2>/dev/null
done

"/cygdrive/c/Program Files (x86)/WinMerge/WinMergeU.exe" -u -r -dl "Repository" -dr "Working" $LOCALAPPDATA\\Temp\\GitDiff\\Repository $LOCALAPPDATA\\Temp\\GitDiff\\Working &
