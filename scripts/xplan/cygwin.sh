# windows program aliases
alias python27=/cygdrive/c/Python27/python
alias python=/cygdrive/c/Python27/python
alias pip=/cygdrive/c/Python27/Scripts/pip.exe
#alias chrome=`cygpath "C:\Users\tpaton\AppData\Local\Google\Chrome\Application\chrome.exe"`
alias chrome="/cygdrive/c/Program\ Files\ \(x86\)/Google/Chrome/Application/chrome.exe"
#alias easy_install="/cygdrive/c/Python27/Scripts/easy_install.exe"
#alias bpython="/cygdrive/c/Python27/Scripts/bpython.exe"
#alias ipython="/cygdrive/c/Python27/Scripts/ipython.exe"
alias sqlcmd="/cygdrive/c/Program\ Files/Microsoft\ SQL\ Server/110/Tools/Binn/SQLCMD.EXE"

# paths for running xplan/bin scripts
#export ARCH=win32-x86 ;
#export LOCATION=MELBOURNE ;
#export PYTHON=/cygdrive/c/Python27/python ;
#export XPLANBASE=/cygdrive/c/xplanbase/version/2.9.999 ;
#export STANDALONE_BUILTLIBS=true ;
#export BUILTLIBSBASE=/cygdrive/c/xplanbase/build ; 
#export PYTHONPATH=`cygpath -wp "${XPLANBASE}/bin:${XPLANBASE}/src/py:${XPLANBASE}/lib/py:${XPLANBASE}/tools/py:${XPLANBASE}/test/py:${XPLANBASE}/lib/arch/${ARCH}:${BUILTLIBSBASE}/vc100/omniORB-4.1.6/bin/x86_win32:${BUILTLIBSBASE}/vc100/omniORB-4.1.6/lib/x86_win32:${BUILTLIBSBASE}/vc100/omniORB-4.1.6/lib/python/"` ;
#export PATH=${BUILTLIBSBASE}/vc100/patches/bin:${PATH}:${XPLANBASE}/lib/arch/${ARCH}:${BUILTLIBSBASE}/vc100/omniORB-4.1.6/bin/x86_win32:${BUILTLIBSBASE}/vc100/omniORB-4.1.6/lib/x86_win32:${BUILTLIBSBASE}/vc100/xerces-c-3.1.1/Build/Win32/VC10/Static\ Release:${BUILTLIBSBASE}/vc100/xerces-c-3.1.1/Build/Win32/VC10/Static\ Debug/ ;
#export OMNIORB_CONFIG=`cygpath -w "${XPLANBASE}/data/omniorb.cfg"` ;
#alias reloadpy="python27 ./bin/reloader.py -d \"var=C:\\xplanbase\\var\\trunk\" -P" # xpt.supersolver.protocol

alias gitdiff='/cygdrive/c/cygwin64/home/tpaton/dotfiles/scripts/xplan/gitdiff.sh'

alias osc='python "c:/Users/tpaton/AppData/Roaming/Python/Scripts/osc"'

alias sfood='python c:/Python27/Scripts/sfood'
alias sfood-checker='python c:/Python27/Scripts/sfood-checker'
alias sfood-cluster='python c:/Python27/Scripts/sfood-cluster'
alias sfood-copy='python c:/Python27/Scripts/sfood-copy'
alias sfood-filter-stdlib='python c:/Python27/Scripts/sfood-filter-stdlib'
alias sfood-flatten='python c:/Python27/Scripts/sfood-flatten'
alias sfood-graph='python c:/Python27/Scripts/sfood-graph'
alias sfood-imports='python c:/Python27/Scripts/sfood-imports'
alias sfood-target-files='python c:/Python27/Scripts/sfood-target-files'

alias dot='/cygdrive/c/Program\ Files\ \(x86\)/Graphviz2.38/bin/dot.exe'
alias winpdb='/cygdrive/c/Python27/Scripts/winpdb.bat'
alias x='./xplan'

import_graph ()
{
    sfood --internal $1 | sfood-graph > $2.dot
    dot -T pdf < $2.dot > $2.pdf
}

np ()
{
    path=""
    if [[ $1 ]] ; then
        path=$(cygpath -aw $1)
    fi
    "/cygdrive/c/Program Files (x86)/Notepad++/notepad++.exe" $path &
    disown
}

#gvim ()
#{
#    path=""
#    if [[ $1 ]] ; then
#        path=$(cygpath -aw $1)
#    fi
#    "/cygdrive/c/Program Files (x86)/Vim/vim73/gvim.exe" --remote $path & 
#    disown
#}
#alias vim=gvim
#alias vi=gvim

chrome_file ()
{
    if [[ $1 == "--path" ]]; then
        shift
        path="$1"
        file="$2"
    else
        path=`pwd`
        file="$1"
    fi
    chrome "file:///c|/cygwin64$path/$file"
}

explorer ()
{
    path=""
    if [[ $1 ]] ; then
        path=$(cygpath -aw $1)
    fi
    /cygdrive/c/Windows/explorer.exe $path &
    disown
}

windiff ()
{
    "/cygdrive/c/Program Files (x86)/WinMerge/WinMergeU.exe" "$(cygpath -w "$1")" "$(cygpath -w "$2")"
}
alias winmerge=windiff

# tortoise svn guis
# st/status/repostatus --> Check for modifications...
# log
# commit
tsvn ()
{
    cmd="$1"
    pth="$2"
    if [[ -z "$cmd" || "$cmd" == "status" || "$cmd" == "st" ]]; then
        cmd="repostatus"
    fi
    
    [[ -z "$pth" ]] && pth="."
    
    /cygdrive/c/Program\ Files/TortoiseSVN/bin/TortoiseProc.exe /command:$cmd /path:$pth &
    disown
}

pylint ()
{
    # less noisy parseable output
    /cygdrive/c/Python27/Scripts/pylint.exe -f parseable --reports=n --include-ids=y $1 2>/dev/nul
}

pylint_path_for_warning ()
{
    # $1 - path to search
    # $2 - warning id
    #       E0602 - undefined variable
    #       W0102 - dangerous function param default
    #       W0311, W0312 - bad indent
    #       W0301 - unnecessary semicolon
    find $1 -iname "*.py" | while read file ; do pylint $file | grep -i $2 ; done
}

pylint_path ()
{
    find $1 -name "*.py" \
         -exec /cygdrive/c/Python27/Scripts/pylint.bat \
         --reports=n \
         --msg-template="{path}:{line}: [{msg_id}({symbol}), {obj}] {msg}" \
         -d C0303 \
         {} \;
}

alias acknp="/cygdrive/c/Python27/python.exe c:/cygwin64/home/tpaton/dotfiles/scripts/xplan/acknp.py"

# teamcity flake8 plugin breaks flake8 somehow, but make depends keeps putting
# it back, so must fix the file whenever flycheck stops working in emacs
fix_flake8 ()
{
    sed -e '/^P999 =/ s/^#*/#/' -i /cygdrive/c/Python27/Lib/site-packages/teamcity_messages-1.18-py2.7.egg/EGG-INFO/entry_points.txt
}
