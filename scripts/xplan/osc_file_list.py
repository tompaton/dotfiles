#!/bin/env python
"""list all files touched by a set of OSC items."""

import re, os, subprocess, urllib2

OSC_COOKIE = 'MANTIS_STRING_COOKIE=c23594d68dd9e988be6bc9724ef3e1b25186faffb0b9fe46ac97519c4dd02584'

def svn_revision_files(svn_path, base_path, rev):
    """Return added or modified files in the given revision"""
    print "Fetching SVN log for revision %s..." % rev
    output = subprocess.Popen(["svn", "log", "-v", "-r%s" % rev, base_path],
                              stdout=subprocess.PIPE).communicate()[0]

    checkfile = False
    files = []
    for line in output.split("\n"):
        if checkfile:
            if line=='\r':
                checkfile = False
            elif line.strip()[0] in ['M','A']:
                print line
                filename = ' '.join(line.strip().split()[1:])
                filename = filename.replace("/","\\")
                filename = filename.replace(svn_path, base_path+"\\")
                files.append(filename)
        else:
            if line=='Changed paths:\r':
                checkfile = True
    return files
    
def get_osc_revisions(osc):
    """Scrape the view osc page for svn revision numbers"""
    print "Getting OSC %s details..." % osc
    response = urllib2.urlopen(urllib2.Request('http://osc.iress.com.au/view.php?id=%s'%osc, "", 
                               { 'Cookie' : OSC_COOKIE }))
    
    return set(re.findall(r"/trac/changeset/(\d+)", response.read()))

filenames = set()    
for osc in "69073".split():
    for revision in get_osc_revisions(osc):   
        for filename in svn_revision_files("\\branches\\cpiece-2-0\\xplan\\", "C:\\xplanbase\\version\\2.0.999", revision):
            filenames.add(filename)

print "\nModified filenames:\n", '\n'.join(sorted(filenames))

