#!/bin/bash
# setup c:\cygwin64\home\tpaton

cd ~/dotfiles
./create-links

cd ~
mkdir bash_functions
cd bash_functions
ln -s /home/tpaton/dotfiles/scripts/aliases.sh               00_aliases.sh
ln -s /home/tpaton/dotfiles/scripts/xplan/bash_functions.sh  10_bash.sh
ln -s /home/tpaton/dotfiles/scripts/xplan/cygwin.sh          20_cygwin.sh
ln -s /home/tpaton/dotfiles/scripts/xplan/branches.sh        30_branches.sh
ln -s /home/tpaton/dotfiles/scripts/xplan/xplan.sh           40_xplan.sh
ln -s /home/tpaton/dotfiles/scripts/lunch/lunch_functions.sh 90_lunch.sh

cd ~
ln -s /cygdrive/c/Users/tpaton/Desktop    Desktop
ln -s /cygdrive/c/xplanbase               xplanbase

mkdir git_home
#ln -s /home/tpaton/dotfiles/scripts/git_bin git_home/bin
# in cmd.exe:
# cd c:\cygwin64\home\tpaton\git_home
# mklink /J bin c:\cygwin64\home\tpaton\dotfiles\scripts\git_bin
# mklink /J .ssh c:\cygwin64\home\tpaton\.ssh
