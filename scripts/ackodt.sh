#!/bin/bash
# run ack on odt files

p='.'
if [[ $1 == '-p' ]] ; then
    p=$2;
    shift; shift;
fi

echo "Scanning $p ..."
for odt in `find $p -name "*.odt"`; do
    ack_results=`unzip -p "$odt" | tidy -q -xml -raw -f /dev/null | grep --color $@`
    if [ -n "$ack_results" ]; then
        echo $odt
        echo $ack_results | sed 's/&lt;/</g; s/&gt;/>/g; s/&amp;/&/g'
        echo
    fi
done
