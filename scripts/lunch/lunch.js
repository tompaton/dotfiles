function table_to_chart(index, table) {
    var data = [];
    $('tr', table).each(function(i) {
        if(i > 0) { // skip header row
            data[i-1] = [];
            data[i-1].name = $(this).find('th').text();
            // stretch cost/size to make more comparable
            switch(data[i-1].name) {
                case '$$$':
                case 'lrg':
                    var factor = 3;
                    break;
                case '$$':
                case 'med':
                    var factor = 2;
                    break;
                case '$':
                case 'sml':
                default:
                    var factor = 1;
            }
            $(this).find('td').each(function(j) {
                if(j > 0) { // skip first td column as it's totals
                    data[i-1][j-1] = {x: j-1, y: $(this).text()*factor};
                }
            });
        }
    });
    var n = data.length, m = data[0].length;
    //var data0 = d3.layout.stack().offset("wiggle")(data);
    var data0 = d3.layout.stack()(data);
    
    var color = d3.scale.category20c();
    //var color = d3.interpolateRgb("#ccf", "#556");

    var width = 600,
        height = 200,
        mx = m - 1,
        my = d3.max(data0, function(d) {
          return d3.max(d, function(d) {
            return d.y0 + d.y;
          });
        });

    var area = d3.svg.area()
        .x(function(d) { return d.x * width / mx; })
        //.y0(function(d) { return height - d.y0 * height / my; })
        //.y1(function(d) { return height - (d.y + d.y0) * height / my; });
        .y0(function(d) { return d.y0 * height / my; })
        .y1(function(d) { return (d.y + d.y0) * height / my; });

    $(table).before('<div class="clear"/>').before('<div id="chart'+index+'" class="chart"/>');
    
    var vis = d3.select('#chart'+index)
      .append("svg")
        .attr("width", width)
        .attr("height", height);

    vis.selectAll("path")
        .data(data0)
      .enter().append("path")
        .style("fill", function() { return color(Math.random()); })
        .attr("d", area)
      .append("svg:title")
        .text(function(d) {
            return d.name;
        });
}

$('table').each(table_to_chart);
