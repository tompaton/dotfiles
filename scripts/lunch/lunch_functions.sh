lunch ()
{
    N=150
    if [[ $1 == "-n" ]]; then
        shift;
        N=$1;
        shift;
    fi

    if [[ ! -z $1 ]]; then
        # log today's lunch - $1 - item (quoted), $2 - M)eat or V)eg, $3 - size: sml,med,lrg, $4 - cost: $ - $$$
        echo "`date +%F`,\"$1\",$2,$3,$4" >> ~/lunch.csv
        echo "$@ logged."
    else
        tail -n $N ~/lunch.csv | awk -F , '{gsub(/[$]/,"\\$", $5); printf("lunch %-20s %s %s %s\n", $2, $3, $4, $5)}' | sort | uniq
    fi
}
lunch_undo ()
{
    removed=$(tail -n 1 ~/lunch.csv)
    sed -i '$d' ~/lunch.csv
    echo "Removed $removed"
}
lunch_recent ()
{
    tail ~/lunch.csv
}
lunch_stats ()
{
    lunch_stats_group 3 . 20
    lunch_stats_group 5 . 20
    lunch_stats_group 4 . 20
    lunch_stats_group 2 . 20
    lunch_stats_total 20
}
lunch_stats_group ()
{
    awk -F , '
      {
        gsub(/"/,"",$2); # strip quotes from description
        
        # convert date to week
        gsub(/-/," ",$1);
        yw=strftime("%Y-%W", mktime($1 " 0 0 0"));
        
        yws[yw] = yw; # list of weeks
        
        # total per week and overall
        count[$(COL)][yw]++;
        total[$(COL)]++
      }
      END {
        # sort weeks (clears keys)
        n=asort(yws);
        
        #printf("%20s : %3s :", "", "");
        #for(i=1;i<=n;i++) {
        #  printf(" %2s", yws[i]);
        #}
        #printf("\n");
        
        ellipsis = "...";
        for(f in count) {
          printf("%20s : %3d :", f, total[f]);
          
          # print week counts
          remainder = 0;
          for(i=n;i>0;i--) {
            c = count[f][yws[i]];
            if(n-i > MAXCOLS) {
                remainder += c;
            } else {
                if(c==0) {
                    printf("  %s", ZERO);
                } else {
                    printf(" %2d", c);
                }
            }
          }
          if(remainder > 0) {
            printf("  %s %3d", ellipsis, remainder);
            ellipsis = "   ";
          }
          
          printf("\n");
        }
        printf("\n");
      }' COL=$1 ZERO=$2 MAXCOLS=$3 ~/lunch.csv | sort -b
}
lunch_stats_total ()
{
    awk -F , '
      {
        # convert date to week
        gsub(/-/," ",$1);
        yw=strftime("%Y-%W", mktime($1 " 0 0 0"));
        yws[yw] = yw; # list of weeks
        count[yw]++;  # count per week
      }
      END {
        # sort weeks (clears keys)
        n=asort(yws);
        
        #printf("\n");
        #printf("%20s : %3s :", "", "");
        #for(i=1;i<=n;i++) {
        #  printf(" %2s", yws[i]);
        #}
        
        # print week counts
        remainder = 0;
        printf("\n        Weekly total :      ");
        for(i=n;i>0;i--) {
          c = count[yws[i]];
          if(n-i > MAXCOLS) {
              remainder += c;
          } else {
            printf(" %2d", c);
          }
        }
        if(remainder > 0) {
          printf("  ... %3d", remainder);
        }
        printf("\n");
      }' MAXCOLS=$1 ~/lunch.csv
}

#bash_completion_lunch ()
#{
#    local cur=${COMP_WORDS[COMP_CWORD]}
#    local wrds=`awk -F , 'BEGIN {OFS="\n";} {gsub(/"/,"",$2);$1="";print}' lunch.csv | sort | uniq | sed '/^$/d'`
#    COMPREPLY=( $(compgen -W "$wrds" -- $cur) )
#}
#complete -F bash_completion_lunch lunch

lunch_stats_table ()
{
    (echo '<table><tr><th>' ;
     echo "$2" ;
     echo '</th><th>Total</th><th colspan=99>Week</th>' ;
     lunch_stats_group $1 0 999 | sed 's/^\s\+/<tr><th>/;
                                       s/ :/<\/th>/;
                                       s/ : / /;
                                       s/ \([0-9]\+\)/<td>\1<\/td>/g;
                                       s/$/<\/tr>/;' ; 
     echo '</table>') >> ~/dotfiles/scripts/lunch/lunch.html
}

lunch_html ()
{
    cp ~/dotfiles/scripts/lunch/lunch_header.html ~/dotfiles/scripts/lunch/lunch.html
    lunch_stats_table 3 "Meat/Veg"
    lunch_stats_table 5 "Cost"
    lunch_stats_table 4 "Size"
    lunch_stats_table 2 "Food"
    cat ~/dotfiles/scripts/lunch/lunch_footer.html >> ~/dotfiles/scripts/lunch/lunch.html
    chrome_file --path "/home/tpaton" dotfiles/scripts/lunch/lunch.html
}
