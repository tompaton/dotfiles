#!/bin/env python
"""get svn diff and compare added functions to removed functions"""
#import pdb; pdb.set_trace()
import sys
import subprocess
import re
from collections import defaultdict

if sys.argv[1:2] == ['__GIT__']:
    cmd = ['git', 'diff'] + sys.argv[2:]
else:
    cmd = ['svn', 'diff'] + sys.argv[1:]
svn_diff = subprocess.Popen(cmd, stdout=subprocess.PIPE).communicate()[0]

defn1 = "\W?((?:def|class)\s+\w+)[:\(]"  # python def and class
defn2 = "\W?(function(?:\s+\w+)?)\("  # javascript function
is_defn = re.compile('(?:%s)|(?:%s)' % (defn1, defn2))

def get_defn(line):
    for defn in is_defn.findall(line):
        # two groupings in the pattern, pick whichever one matched
        yield defn[0] or defn[1]

new, old = defaultdict(list), defaultdict(list)
for line in svn_diff.split("\n"):
    if line[:3] in ('---', '+++'):
        continue
    for defn in get_defn(line):
        if line[0] == '-':
            old[defn].append(line[1:].strip())
        if line[0] == '+':
            new[defn].append(line[1:].strip())

added, removed, changed = [], [], []
for defn in new.keys():
    if defn in old:
        # convert to a single string so they stay together when sorted
        changes = "".join(
            ["\t%s\n" % line for line in old[defn]]
            + ["\t %s\n" % (line.replace(defn, '-'*(len(defn)-3)+'> '))
               for line in new[defn]])
        # strip off initial tab & trailing new line
        changed.append(changes[1:-1])
    else:
        added.extend(new[defn])
for defn in old.keys():
    if not defn in new:
        removed.extend(old[defn])

for title, lines in (("Added:", added),
                     ("Changed:", changed),
                     ("Removed:", removed)):
    if lines:
        print title
        print "".join('\t%s\n' % line for line in sorted(lines))
    else:
        print title, 'none.'

if removed:
    print "ack '(%s)'" % '|'.join(defn.split(" ")[1]
                                  for line in removed
                                  for defn in get_defn(line)
                                  if " " in defn)
