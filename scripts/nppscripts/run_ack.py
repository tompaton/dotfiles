"""Run ack-grep and output to the console"""

ack = notepad.prompt("Search string:","Ack-grep")
if ack:
    console.show()
    #console.clear()
    console.write('\n\nAck-grep %s\n'%ack)
    cmd = (r'cmd.exe /c "ack.bat '
            + '--ignore-dir=js.min --ignore-dir=css.min --ignore-dir=build --ignore-dir=lib '
            # TODO: --no-group etc.
            + ack + ' '
            # TODO: pick up current branch
            + r'c:\xplanbase\version\2.99.999\"')
    console.write(cmd+"\n")
    console.run(cmd)