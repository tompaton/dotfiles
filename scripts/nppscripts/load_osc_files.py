"""Notepad++ Python Script to open all files associated with an OSC item."""

import re, os
import urllib2
from toms_utils import svn_revision_files, open_file

# Login in Chrome & grab this with document.cookie, expires yearly
OSC_COOKIE = 'MANTIS_STRING_COOKIE=c23594d68dd9e988be6bc9724ef3e1b25186faffb0b9fe46ac97519c4dd02584'
    
def get_osc_revisions(osc):
    """Scrape the view osc page for svn revision numbers"""
    console.write("Getting OSC %s details...\n"%osc)
    response = urllib2.urlopen(urllib2.Request('http://osc.iress.com.au/view.php?id=%s'%osc, "", 
                               { 'Cookie' : OSC_COOKIE }))
    
    return set(re.findall(r"/trac/changeset/(\d+)", response.read()))
    
def get_osc_files(svn_path, base_path, osc):
    """Set of all files touched by all revisions associated with the given OSC item"""
    filenames = set()
    for revision in get_osc_revisions(osc):   
        for filename in svn_revision_files(svn_path, base_path, revision):
            filenames.add(filename)
    return filenames

osc = notepad.prompt("OSC ID","Open files for OSC item")
if osc:    
    for filename in get_osc_files("\\trunk\\xplan\\", "C:\\xplanbase\\version\\2.99.999", osc):
        open_file(filename)

    console.write("Done.\n")
        