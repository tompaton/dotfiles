"""Utility functions for Notepad++ Python Scripts."""

import re, os, subprocess
from Npp import notepad, editor, console, FINDOPTION

def split_branch(filename):
    """Split the given filename into base,branch,filename components."""
    matches = re.findall(r'^(.+)\\(\d\.\d+\.\d+)\\(.+)$', filename)
    if matches:
        return matches[0]
    else:
        return ('','',filename)

def get_version_path(filename):
    """Find the base path of the site source based on the filename"""
    return '%s\\%s\\' % split_branch(filename)[:2]
    
def svn_revision_files(svn_path, base_path, rev):
    """Return added or modified files in the given revision"""
    console.write("Fetching SVN log for revision %s...\n"%rev)
    output = subprocess.Popen(["C:\\Program Files\\TortoiseSVN\\bin\\svn.exe",
                               "log","-v","-r%s"%rev,
                               base_path],
                              stdout=subprocess.PIPE).communicate()[0]

    checkfile = False
    files = []
    for line in output.split("\n"):
        if checkfile:
            if line=='\r':
                checkfile = False
            elif line.strip()[0] in ['M','A']:
                filename = ' '.join(line.strip().split()[1:])
                filename = filename.replace("/","\\").replace(svn_path, base_path+"\\")
                files.append(filename)
        else:
            if line=='Changed paths:\r':
                checkfile = True
    return files
    
def prompt_select_branches(prompt, base_path, exclude=None):
    """Popup a dialog with a list of all the branch folder names.
       Edit the list to include only the desired branches for the operation.
       Returns a list of selected branch paths."""
       
    if exclude is None: exclude = []
    
    # list of branches in base_path
    branches = [f for f in os.listdir(base_path) 
                      if os.path.isdir(os.path.join(base_path, f)) 
                      and re.match(r'^\d\.\d+\.\d{2,}$', f) # only want 2.x.yy branches, not 2.0.6
                      and f not in exclude]

    # prompt (edit to just desired branch(es))
    selected_branches = notepad.prompt("Branch(es)", prompt, ", ".join(branches))
    
    return ['%s\\%s' % (base_path, branch.strip()) for branch in (selected_branches or '').split(",")]

def open_file(filename, symbol=None, line=None):
    """Open the given file and activate it."""
    if os.path.splitext(filename)[1].lower() in ['.py', '.html', '.js', '.css', '.txt', '.xml', '.sql']:
        notepad.open(filename)
        notepad.activateFile(filename)

        if symbol is None:
            if line is None:
                console.write("%s\n"%(filename))
            else:
                console.write("%s:%d\n"%(filename, line))
                editor.gotoLine(line-1)
                editor.setFirstVisibleLine(max(0, line-10))
        else:
            try:
                pos = editor.getText().index(symbol)
            except ValueError: 
                console.write("%s (%s not found)\n"%(filename, symbol))
            else:
                editor.gotoPos(pos)
                editor.setSelection(pos, pos+len(symbol))
                editor.setFirstVisibleLine(max(0, editor.lineFromPosition(pos)-10))
                console.write("%s:%d %s\n"%(filename, editor.lineFromPosition(pos), symbol))
    else:
        console.write("%s (Skipped)\n" % filename)

def goto_next_occurrence():
    """Move cursor to next occurrence of the current word in the file, wrap around if possible."""
    symbol = editor.getCurrentWord()
    editor.wordRight()
    editor.searchAnchor()
    pos = editor.searchNext(FINDOPTION.MATCHCASE+FINDOPTION.WHOLEWORD+FINDOPTION.WORDSTART, symbol)
    editor.scrollCaret()

def goto_prev_occurrence():
    """Move cursor to previous occurrence of the current word in the file, wrap around if possible."""
    symbol = editor.getCurrentWord()
    editor.wordLeft()
    editor.searchAnchor()
    pos = editor.searchPrev(FINDOPTION.MATCHCASE+FINDOPTION.WHOLEWORD+FINDOPTION.WORDSTART, symbol)
    editor.scrollCaret()
