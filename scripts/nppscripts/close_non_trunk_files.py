"""Notepad++ Python Script to close all files from non-trunk branches."""

from toms_utils import split_branch

for filename, _, _, _ in notepad.getFiles():
    base, branch, name = split_branch(filename)
    if base.lower() == r'c:\xplanbase\version' and branch != '2.99.999':
        console.write("%s Closed\n" % filename)
        notepad.activateFile(filename)
        notepad.close()