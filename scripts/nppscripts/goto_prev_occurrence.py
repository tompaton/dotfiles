"""Notepad++ Python Script to goto the previous occurrence of the current word."""
from toms_utils import goto_prev_occurrence
goto_prev_occurrence()
