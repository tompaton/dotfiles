"""Notepad++ Python Script to prompt for an SVN revision and copy all touched files to another branch."""

import shutil
from toms_utils import svn_revision_files, prompt_select_branches, split_branch, open_file

revision = notepad.prompt("Revision number to backport","SVN Revision","")
if revision:
    branches = prompt_select_branches('Copy files to another branch', "C:\\xplanbase\\version", exclude=['2.99.999'])
    svn_files = svn_revision_files("\\trunk\\xplan\\", "C:\\xplanbase\\version\\2.99.999", revision)
    for dest_branch in branches:
        console.write('Copying to %s...\n' % (dest_branch))
        for base_path, branch, filename in map(split_branch, svn_files):
            src = "%s\\%s\\%s" % (base_path, branch, filename)
            dest = "%s\\%s" % (dest_branch, filename)
            if src != dest:
                shutil.copyfile(src, dest)
                open_file(dest)

    console.write("Done.\n")