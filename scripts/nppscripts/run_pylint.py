"""Run pylint on the current window"""

# TODO: set python path so xpt.* imports succeed.

def pylint_to_console():
    """Simple output of pylint to console with hyperlinks to lines"""

    console.show()
    console.clear()
    console.run('cmd.exe /c '
                + 'C:\\Python27\\Scripts\\pylint.bat --reports=n -f parseable '
                + '--max-line-length=150 '
                + '"%s"' % notepad.getCurrentFilename())

def annotate_with_pylint(hide=False):                
    """Fancy pylint output as annotations in buffer"""

    editor.annotationClearAll()

    if hide:
        editor.annotationSetVisible(0)
        return

    style_map = {'C':64, 'W':65, 'E':66, 'R':67, 'F':68}
    for style in style_map.values(): 
        editor.styleSetItalic(style, True)
        editor.styleSetFore(style, (128, 128, 128))

    editor.styleSetBack(64, (224, 224, 224))
    editor.styleSetBack(65, (224, 255, 224))
    editor.styleSetBack(66, (255, 224, 224))
    editor.styleSetBack(67, (255, 224, 224))
    editor.styleSetBack(68, (255, 224, 224))

    import subprocess
    output = subprocess.Popen(["cmd.exe", "/c",
                               "C:\\Python26\\Scripts\\pylint.bat",
                               "--reports=n", "-f", "parseable",
                               "--max-line-length=100",
                               notepad.getCurrentFilename()],
                              stdout=subprocess.PIPE).communicate()[0]

    import re
    for line in output.split("\n"):
        #console.write(line+"\n")
        for filename, lineno, issue, desc, message in re.findall(r'(.+):(\d+):\s+\[(\w)(,\s.+)?\]\s+(.+)', line.strip()):
            # TODO: handle multiple styles per annotation properly
            annotation = '[%s%s] %s' % (issue, desc, message)
            styles = chr(style_map[issue]) * len(annotation)
            existing = editor.annotationGetText(int(lineno)-1)
            if existing:
                annotation += "\n"+existing
                styles += "\x00"+editor.annotationGetStyles(int(lineno)-1)
            editor.annotationSetText(int(lineno)-1, annotation)
            editor.annotationSetStyles(int(lineno)-1, styles)

    editor.annotationSetVisible(1)
            
pylint_to_console()
#annotate_with_pylint(hide=editor.annotationGetVisible())
