"""Notepad++ Python Script to attempt to find the definition of a python/javascript function."""
import re, os
from toms_utils import open_file

def jump_to_def(cur_word):
    """Find the definition of the current function & go to it."""
    
    matched_lineno = []
    def matched_line(lineno, match):
        matched_lineno.append(lineno)
    
    for (filename, bufferID, index, view) in notepad.getFiles():
        notepad.activateBufferID(bufferID)
        editor.pysearch(r'^\s*(function|def)\s+%s\(' % cur_word, matched_line)
        if matched_lineno:
            editor.gotoLine(matched_lineno[0])
            return True
    
    return False

bufferID = notepad.getCurrentBufferID()
start = editor.getCurrentPos()
if not jump_to_def(editor.getCurrentWord()):
    notepad.activateBufferID(bufferID)
    editor.gotoPos(start)

