"""List all $vars in the current template"""

import re

console.show()
console.write('\nTemplate variables in %s:\n' % notepad.getCurrentFilename())

for var in sorted(set(re.findall(r'\$(\w+)', editor.getText()))):
    console.write('\t%s\n' % var)
