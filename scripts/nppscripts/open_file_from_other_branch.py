"""Notepad++ Python Script to open version of current source file from another branch."""

from toms_utils import split_branch, prompt_select_branches, open_file

base, branch, filename = split_branch(notepad.getCurrentFilename())

for other in prompt_select_branches('Open file from another branch', base, exclude=[branch]):
    open_file('%s\\%s' % (other, filename))
