"""Notepad++ Python Script to open another source file based on the current line/word."""
import re, os
from toms_utils import open_file, get_version_path

def jump_to_other_file(base_path, cur_line, cur_word):
    """Various heuristics to identify the current reference to another file & open it."""
    
    # see if this is a line in a ack/pylint output
    for file_path, line_number in re.findall(r'^([\w\\\/\.]+):(\d+):', cur_line):
        # assume we're in trunk
        console.write('Matched path %s line %s ...\n' % (file_path, line_number))
        open_file(('C:\\xplanbase\\version\\2.99.999\\' if base_path == '\\\\' else base_path)+file_path.replace("/","\\"), line=int(line_number))
        return
    
    # see if this is a line in a server.log Traceback
    # File "C:\xplanbase\version\2.99.999\src\py\xpt\supersolver\key_features.py", line 333, in get_key_feature_search
    for file_path, line_number in re.findall(r'^  File "([\w\\\/\.:]+)", line (\d+),?', cur_line):
        console.write('Matched path %s line %s ...\n' % (file_path, line_number))
        open_file(file_path.replace("/","\\"), line=int(line_number))
        return
    # Error: <Error: Syntax Error: invalid syntax (supersolver/settings/admin_super_apl.html, line 76)>
    for file_path, line_number in re.findall(r'^Error: <.+\((.+), line (\d+)\)>', cur_line):
        # assume we're in trunk
        console.write('Matched path %s line %s ...\n' % (file_path, line_number))
        open_file('C:\\xplanbase\\version\\2.99.999\\data\\ihtml\\' + file_path.replace("/","\\"), line=int(line_number))
        return
    
    # find a python from ... import ...
    for module, function in re.findall(r"^\s*from\s+([\w\.]+)\s+import\s+([\w\.\,\s]+)\s*\\?\s*#?.*$", cur_line):
        console.write("Following Python from ... import ...\n")
        module = base_path+'src\\py\\'+module.replace('.','\\')+'.py'
        function = function.strip() if len(function.split(','))==1 and len(function.split())==1 else cur_word
        if os.path.exists(module):
            # from module import symbol
            open_file(module, symbol = function)
        else:
            # from path import file
            open_file(module.replace(".py", '\\%s.py' % function))
    
    # find a python import ...
    for module in re.findall(r"^\s*import\s+([\w\.]+)\s*(?:as\s+\w+)?\s*$", cur_line):
        console.write("Following Python import ...\n")
        open_file(base_path+'src\\py\\'+module.replace('.','\\')+'.py')
        return
    
    # find a javascript file from .py or .html
    if re.search(r"\b%s\.js"%cur_word, cur_line):
        console.write("Opening .js file at cursor...\n")
        open_file( base_path + 'data\\wwwroot\\js\\' + cur_word+ '.js' )
        return
    
    # find a css file from .py or .html
    if re.search(r"\b%s\.css"%cur_word, cur_line):
        console.write("Opening .css file at cursor...\n")
        open_file( base_path + 'data\\wwwroot\\css\\' + cur_word+ '.css' )
        return
    
    # find a html template
    if re.search(r'get\w*TPO|getMainFrame|setFile|get_full_page_popup_template|get_standard_template|get_popup_template|Template', cur_line):
        #console.write('A> cur_line: %s\n'%cur_line)
        for bits in re.findall(r"\[([^\]]+)\]", cur_line):
            #console.write('bits: %r\n' % bits)
            if '.html' in bits:
                console.write("Following getTPO...\n")
                open_file( base_path + 'data\\ihtml\\' + bits.replace("'","").replace(" ","").replace(",","\\") )
                return
    # find engage template
    if "init_engage_tpl" in cur_line:
        for bits in re.findall(r"[\"']([^\"']+)[\"']", cur_line):
            if '.html' in bits:
                console.write("Following getTPO...\n")
                open_file( base_path + 'data\\ihtml\\' + bits.replace("/","\\") )
                return
    # find included template
    if "<:include " in cur_line:
        for bits in re.findall(r"<:include ([\w\./]+):>", cur_line):
            if '.html' in bits:
                console.write("Following <:include:>...\n")
                open_file( base_path + 'data\\ihtml\\' + bits.replace("/","\\") )
                return
    if re.search(r'get_popup_template|get_full_page_popup_template|Template', cur_line):
        #console.write('B> cur_line: %s\n'%cur_line)
        for bits in re.findall(r"\([\"']([\w\./]+)[\"']", cur_line):
            #console.write('bits: %r\n' % bits)
            if '.html' in bits:
                console.write("Following get_popup_template...\n")
                open_file( base_path + 'data\\ihtml\\' + bits.replace("/","\\") )
                return
    
    # find a js depedency
    if "addJS" in cur_line:
        for js in re.findall(r".+\(\s*['\"](.+)['\"]\).+", cur_line):
            console.write("Following Dependency.addJS...\n")
            open_file( base_path + 'data\\wwwroot\\js\\' + js.replace("/","\\") + '.js' )
            return
    
    # find an rpc handler from .js
    if "callJSON" in cur_line:
        for module, function in re.findall(r".+callJSON\(\s*['\"](\w+)\.(\w+)['\"].*", cur_line):
            console.write("Following callJSON...\n")
            if module in ('iqm2', 'rr', 'rr_sg', 'rr_gb'):
                module = 'insurance\\%s' % module
            open_file(base_path+'src\\py\\xpt\\'+module+'\\rpc.py',
                      symbol = "rpc_"+function+"(")
            return
    
    # try matching a url to find a protocol handler from .html/.js
    for module1, module2, function in re.findall(r"/(\w+)/(\w+)/(\w+)", cur_line):
        console.write('Matched url /%s/%s/%s ...\n' % (module1, module2, function))
        open_file(base_path+'src\\py\\xpt\\'+module1+'\\'+module2+'.py',
                  symbol = "req_"+function+"(")
        return
    for module, function, remainder in re.findall(r"/([\w\+]+)/(\w+)/?(\w*)", cur_line):
        console.write('Matched url /%s/%s ...\n' % (module, function))
        if module == 'iqm+':
            if function in ('rr', 'rr_sg'):
                module = function
                function = remainder
            else:
                module = 'iqm2'
        if module in ('iqm2', 'rr', 'rr_sg'):
            module = 'insurance\\%s' % module
        open_file(base_path+'src\\py\\xpt\\'+module+'\\protocol.py',
                  symbol = "req_"+function+"(")
        return

jump_to_other_file(get_version_path(notepad.getCurrentFilename()),
                   editor.getCurLine(), editor.getCurrentWord())
