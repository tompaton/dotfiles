# Description: cyclesbot dev env

cd ~/dev/bots/cyclesbot

workon cyclesbot

dev_env ()
{
    emacs bot.py &
    disown

    thg &
    disown
}

run_tests ()
{
    python -m pytest $@
}

run_coverage ()
{
    coverage run --source=. --branch -m pytest $@
    coverage report -m --skip-covered
}

check_types ()  # or `check_types --strict`
{
    if [ -z "$1" ]; then
        modules=$(find . -maxdepth 1 -type d \
                       -exec test -f {}/__init__.py \; \
                       -printf "%f/ ")
        modules="bot.py $modules"
    else
        modules="$@"
    fi

    mypy --config-file mypy.ini $modules
}
