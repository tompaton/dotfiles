#!/bin/bash
# Description: photobook dev env

cd ~/dev/flask_projects/photobook

workon photobook

emacs photobook/photobook.py &
disown

thg &
disown

sqlitebrowser photobook.db &
disown

# setsid to start in new process group, trap exit and kill process groups of jobs
# setsid ./devel_server.py &
# setsid npm run watch &
# trap 'for p in $(jobs -p) ; do kill -- -$p ; done' EXIT
#./devel_server.py &
#npm run watch &

chromium-browser http://127.0.0.1:5000 &
disown

alias flow=~/dev/flow/flow

function tag_production ()
{
    read -p "update production tag to current revision? (yes) : "
    if [ "$REPLY" = "yes" ]
    then
        echo "tagging..."
        hg tag -f production
    fi
}

function deploy ()
{
    #remote="tom@tompaton.com"
    remote="tom@magnesium"

    remote_app="/var/data/www/photobook.tompaton.com"

    echo "run npm run build && tag_production first."

    read -p "Deploy to linode? (yes) : "
    if [ "$REPLY" = "yes" ]
    then
        echo "Deploying..."
        hg archive -r production --type "tgz" - \
            | ssh $remote "tar xzvf - -C ${remote_app} --strip-components 1"

        # mark each static files with revision - only want url to change if file
        # changes
        cd public
        for f in static/*.{js,css}; do
            rev=$(hg log --limit 1 --template "{rev}" $f)
            echo $f - $rev
            ssh $remote "sed --in-place s,/${f},/static/${rev}/$(basename $f), ${remote_app}/photobook/templates/photobook.html"
        done
        cd -

        # trigger mod_wsgi reload 
        ssh ${remote} touch ${remote_app}/tmp/restart.txt
    fi
}

function get_log_changes_csv ()
{
    hg log --stat | tac \
        | awk 'BEGIN {print "files,insert,delete,delta"}
               /deletions/{print $1 "," $4 "," $6 "," ($4-$6)}' \
              > changes.csv
}

function make_callgrind ()
{
    cd debug
    for f in *.prof ; do
        if [ ! -e "callgrind.$f" ]
        then
            pyprof2calltree -i $f -o callgrind.$f
        fi
    done
    cd -
}
