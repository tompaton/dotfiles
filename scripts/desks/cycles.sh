# Description: cycles dev env

cd ~/dev/flask_projects/cycles

workon cycles

emacs cycles/cycles.py &
disown

# mercurial commit gui window
thg &
disown

# db
#sqlite3 /tmp/cycles.db < schema.sql
#sqlite3 /tmp/cycles.db < fixture.sql

# flask
#export CYCLES_SETTINGS="$HOME/dev/flask_projects/cycles/debug_settings.py"
#python cycles/cycles.py &
./devel_server.py &

# jsx
#rm static/.lock.pid
#rm -rf static/.module-cache
#jsx --watch jsx/ static/ &
#npm run watch &

# chrome
chromium-browser http://127.0.0.1:5000 &
disown

function tag_cycles_production ()
{
    read -p "update production tag to current revision? (yes) : "
    if [ "$REPLY" = "yes" ]
    then
        echo "tagging..."
        hg tag -f production
    fi
}

function build_cycles_container ()
{
    rm -rf ./cycles_build
    hg archive -r production ./cycles_build/

    rm -rf ./cycles_build/jsx

    for f in static/*.{js,css}; do
        local rev=$(hg log --limit 1 --template "{rev}" $f)
        echo $f - $rev
        sed --in-place s,/$f,/static/$rev/$(basename $f), ./cycles_build/templates/cycles.html
    done

    docker build -t tompaton/cycles .
}

alias flow=~/dev/flow/flow
