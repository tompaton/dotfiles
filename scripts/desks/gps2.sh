# Description: gpsloglabs.com dev env and local instance

function gps2_dev_env() {
    #HOST_PORT='192.168.0.2:8000'
    HOST_PORT='localhost:8000'

    if [ -z "$1" ]
    then
        # not given a branch
        branch="gps2-trunk"
    else
        branch="$1"
    fi

    # working directory
    cd /home/tom/dev/django_projects/${branch}

    # emacs (run gps2_server from m-x shell)
    emacs gps2.org &
    disown

    # mercurial commit gui window
    thg &
    disown

    # chrome
    chromium-browser http://$HOST_PORT &

    # activate virtualenv
    workon gps2-django17

    # compile .less files
    # sudo npm install -g less
    # sudo npm install -g watch-less
    # watch-less -d media/css/ -e .css &

    make watch-less &

    update_activity_ics &
}

function gps2() {
    #HOST_PORT='192.168.0.2:8000'
    HOST_PORT='localhost:8000'

    if [ -z "$1" ]
    then
        # not given a branch
        branch="gps2-trunk"
    else
        branch="$1"
    fi

    # working directory
    cd /home/tom/dev/django_projects/${branch}

    # spare terminal
    gnome-terminal &

    # chrome
    chromium-browser http://$HOST_PORT &

    # activate virtualenv
    workon gps2-django17

    update_activity_ics &

    gps2_server $HOST_PORT
}

gps2_server () {
    if [ -z "$1" ]
    then
        #HOST_PORT="192.168.1.99:8000"
        HOST_PORT="localhost:8000"
    else
        HOST_PORT="$1"
    fi

    # activate virtualenv
    workon gps2-django17

    # django dev server
    ./manage.py runserver $HOST_PORT
}

function gps2_upload ()
{
    local gt31="/media/tom/18A6-237B"
    local gt31_dest="/home/tom/Documents/gpslogs - Locosys GT31"
    local garmin="/media/tom/GARMIN/GARMIN"
    local garmin_dest="/home/tom/Documents/gpslogs - Garmin Forerunner 235"

    if [ -d "$gt31/GPS_DATA" ]; then
        cd "$gt31_dest" || exit 1
        python3 "$gt31/uploader_local.py"
        cd -
    fi

    if [ -d "$garmin" ]; then
        cd "$garmin_dest" || exit 1

        # upload logs to local site
        python3 ./uploader.py

        # save settings
        rsync --verbose --archive --recursive --stats --exclude=ACTIVITY/ "$garmin/" .
        hg commit -m "Backed up device settings."

        cd -
    fi
}

function gps2_process ()
{
    # activate virtualenv
    workon gps2-django17

    ./manage.py process_queued_actions --loop
}


function gps2_etags ()
{
    cd ~/dev/django_projects/gps2-trunk
    (find . -name "*.py" -not -path "./build/*" -print ;
     find . -name "*.js" -not -path "./build/*" -print ;
     find . -name "*.html" -not -path "./build/*" -print ) | etags -
    cd -
}

function update_activity_ics ()
{
    local secret="6efcfae1d5a48bb5e77d7244e36980312cfcaae2"
    #local ics_url="http://192.168.1.99:8000/tag/feed/ical/$secret"
    local ics_url="http://localhost:8000/tag/feed/ical/$secret"
    local activity_ics="$HOME/dev/www/sites/tompaton.com/resources/activity.ics"
    local resources="/var/data/www/tompaton.com/resources"

    while [ 1 ]; do
        sleep 300
        echo "Updating activity.ics"
        curl -f $ics_url > $activity_ics \
            && rcp $activity_ics magnesium:$resources/activity.ics
        sleep 300
    done
}
