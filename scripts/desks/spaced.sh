# Description: spaced (emscripten port)

cd ~/dev/Old\ Projects/spaced/src/

source ~/dev/linux-from-source/emsdk_portable/emsdk_env.sh

emacs spaced.cpp &
disown

thg &
disown

emrun --no_browser em2-spaced.html

# TODO: copy to ~/dev/www/sites/tompaton.com/pages/

# TODO: gzip < em2-spaced.data > em2-spaced.data.gz
#       gzip < em2-spaced.js > em2-spaced.js.gz
