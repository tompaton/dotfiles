# Description: photos functions

# copy olympus photos to manual import - obsolete, use get_photos
# copy_olympus_photos()
# {
#     ~/Pictures/Photos/temp/manual_import/copy_olympus_photos.sh
# }

copy_iphotos()
{
    # NOTE: don't forget to update these to get the latest photos
    #_copy_iphotos _IPAD 58c9d4f719acab3a13028022433afa6f2a7928c5/DCIM/102APPLE
    # _copy_iphotos _A5S afc\:host=46d05cb1efcbeaf4410af188905fb8398ed52e4e/DCIM/100APPLE
    _copy_iphotos _IPAD gphoto2\:host=%5Busb%3A001%2C007%5D/DCIM/102APPLE
    _copy_iphotos _A5S gphoto2\:host=%5Busb%3A001%2C005%5D/DCIM/101APPLE
}

_copy_iphotos()
{
    dst="/home/tom/Pictures/Photos/temp/manual_import"
    for src in /run/user/1000/gvfs/$2
    do
        folder=$(basename $src)
        mkdir -p $dst/${folder}$1
        cp --verbose --no-clobber $src/*.JPG $dst/${folder}$1
        cp --verbose --no-clobber $src/*.MOV $dst/${folder}$1
    done
}

# get photos from sd card, copy to original and resize to todo
get_photos()
{
    ~/dev/photos_py/get_photos.py

}
# process batch from todo to final
process_batch()
{
    ~/dev/photos_py/batch.py && (ls ~/Pictures/Photos/todo/ | wc -l)
}

# remove copied photos from manual import
remove_imported_photos()
{
    ~/Pictures/Photos/temp/manual_import/remove_imported_photos.sh
}

# copy original photos into directory of selected final photos
find_originals()
{
    ~/dev/photos_py/find_originals.py
}
